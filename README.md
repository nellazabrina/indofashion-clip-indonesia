# IndoFashion CLIP Indonesia

## Dataset
* Source: https://www.kaggle.com/datasets/validmodel/indo-fashion-dataset 
* All captions & labels are mostly translated by Google Translate (and some by Deepl)
* Training data: only use first 1.000 records for each label
* Test data: only use first 300 records for each label

## Project status
Stopped. Google Colab is weird these past days. I subscribe Google Colab Pro before and now, but with the same code, these days always RAM crash.

## Reference
* Galuh Sahid. (2021). CLIP Model on Indonesian Data. In Talk in Python Conference Indonesia 2021. https://pycon.id/. https://github.com/galuhsahid/clip-indonesian.
* Bianchi F, et al. (2021). Contrastive language-image pre-training for the italian language. arXiv preprint arXiv:2108.08688.
* Radford, A., et al. (2021, July). Learning transferable visual models from natural language supervision.
      In International conference on machine learning (pp. 8748-8763). PMLR.
